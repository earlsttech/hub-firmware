// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

#![no_main]
#![no_std]
#![feature(default_alloc_error_handler)]

extern crate alloc;

use core::time::Duration;
use firmware_common::{
    allocator,
    async_await::{RealTime, YieldingExecutor},
    impl_cyccnt_rt,
};
use rtic::{
    cyccnt::{U32Ext, CYCCNT},
    Monotonic,
};
use stm32f4xx_hal::{
    gpio::{gpioc::PC3, Output, PushPull},
    pac::TIM7,
    prelude::*,
    stm32::Interrupt,
    timer::{Event, Timer},
};

#[cfg(debug_assertions)]
use cortex_m_semihosting::*;

#[cfg(debug_assertions)]
use panic_semihosting as _;

#[cfg(not(debug_assertions))]
use panic_halt as _;

const DELAY: u32 = 168_000_000;

impl_cyccnt_rt!(CYCCNT, CYCCNT_RT, 1 / 168_000_000);

#[rtic::app(device = stm32f4xx_hal::stm32, peripherals = true, monotonic = rtic::cyccnt::CYCCNT)]
const APP: () = {
    struct Resources {
        led: PC3<Output<PushPull>>,
        exec: YieldingExecutor<CYCCNT_RT>,
        timer: Timer<TIM7>,
    }

    #[init(schedule = [blink])]
    fn init(cx: init::Context) -> init::LateResources {
        unsafe { allocator::init() }

        // Cortex-M peripherals
        let mut core = cx.core;
        core.DWT.enable_cycle_counter();

        // Device specific peripherals
        let device = cx.device;

        // Setup clocks
        let rcc = device.RCC.constrain();
        let clocks = rcc.cfgr.use_hse(16.mhz()).sysclk(168.mhz()).freeze();

        // Setup LED
        let gpioc = device.GPIOC.split();
        let mut led = gpioc.pc3.into_push_pull_output();
        led.set_high().unwrap();

        // Schedule the blinking task
        cx.schedule.blink(cx.start + DELAY.cycles()).unwrap();

        #[cfg(debug_assertions)]
        hprintln!("init").unwrap();

        let mut timer = Timer::tim7(device.TIM7, 1, clocks);
        timer.listen(Event::TimeOut);

        init::LateResources {
            led,
            exec: YieldingExecutor::new(Interrupt::TIM7),
            timer,
        }
    }

    #[task(resources = [led], schedule = [blink])]
    fn blink(cx: blink::Context) {
        let _ = cx;
        cx.resources.led.toggle().unwrap();
        cx.schedule.blink(cx.scheduled + DELAY.cycles()).unwrap();
    }

    #[task(binds = TIM7, resources = [&exec, timer], priority = 7)]
    fn exec_step(cx: exec_step::Context) {
        cx.resources.timer.clear_interrupt(Event::TimeOut);

        unsafe {
            cx.resources
                .exec
                .step(cx.resources.timer, |diff| 168_000_000 / (diff as u32));
        }
    }

    #[idle(resources = [&exec])]
    fn idle(cx: idle::Context) -> ! {
        let loop_future = async {
            let mut next = CYCCNT_RT::now();
            loop {
                #[cfg(debug_assertions)]
                hprintln!("loop").unwrap();

                next = CYCCNT_RT::offset(next, Duration::from_secs(1));
                cx.resources.exec.delay_until(next).await;
            }
        };
        unsafe { cx.resources.exec.init_heapless(loop_future) }
    }

    extern "C" {
        fn ETH();
        fn ETH_WKUP();
    }
};
