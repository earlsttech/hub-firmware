use cortex_m_semihosting::{nr::open, syscall};

pub struct HStdin {
    fd: usize,
}

impl HStdin {
    pub fn read(&mut self, buffer: &mut [u8]) -> Result<usize, ()> {
        match unsafe { syscall!(READ, self.fd, buffer.as_mut_ptr(), buffer.len()) } {
            n if n <= buffer.len() => Ok(n),
            _ => Err(()),
        }
    }
}

pub fn hstdin() -> Result<HStdin, ()> {
    match unsafe { syscall!(OPEN, ":tt\0".as_bytes().as_ptr(), open::R, 3) } {
        n if (n as isize == -1) => Err(()),
        fd => Ok(HStdin { fd }),
    }
}
