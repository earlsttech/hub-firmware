// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

use alloc::{format, string::String, vec::Vec};
use itertools::Itertools;

pub type DirectoryPath = Vec<String>;

pub struct Cli {
    cwd: DirectoryPath,
}

impl Cli {
    pub fn new(cwd: DirectoryPath) -> Self {
        Cli { cwd }
    }

    pub fn prompt(&self) -> String {
        format!("{}>", self.cwd.iter().format("/"))
    }
}
