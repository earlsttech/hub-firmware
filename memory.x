# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at http://mozilla.org/MPL/2.0/.

MEMORY
{
    FLASH : ORIGIN = 0x08000000, LENGTH = 1M
    RAM : ORIGIN = 0x10000000, LENGTH = 64K
    RAM2 : ORIGIN = 0x20000000, LENGTH = 128K
}

_heap_start = ORIGIN(RAM2);
_heap_end = ORIGIN(RAM2) + LENGTH(RAM2);
